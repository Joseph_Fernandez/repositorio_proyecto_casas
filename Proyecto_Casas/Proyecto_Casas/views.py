#aqui es donde irian las funciones y objetos para calcular lo requerido en nuestra guia.
class Modelo_Medida(object):

    def __init__(self,baños,habitaciones):
        self.baños = baños
        self.habitaciones = habitaciones
        

    def Tipo_Modelo(self):
        if self.baños == 1 and self.habitaciones == 1:
            valor_modelo = 1000000
        elif self.baños == 1 and self.habitaciones == 2:
             valor_modelo = 1250000
        elif self.baños == 1 and self.habitaciones == 3:
             valor_modelo = 1500000  
        elif self.baños == 1 and self.habitaciones == 4:
             valor_modelo = 1750000
        elif self.baños == 2 and self.habitaciones == 1:
             valor_modelo = 1250000               
        elif self.baños == 2 and self.habitaciones == 2:
            valor_modelo = 1500000
        elif self.baños == 2 and self.habitaciones == 3:
            valor_modelo = 1750000
        elif self.baños == 2 and self.habitaciones == 4:
            valor_modelo = 2000000  
        return valor_modelo   

    def get_baños(self):
        return self.baños

    def get_habitaciones(self):
        return self.habitaciones



from django.template.loader import get_template
from django.http import HttpResponse

def informacion_modelo_medida(request):
    cant_baños = 2
    cant_habitaciones = 3
    obj = Modelo_Medida(cant_baños,cant_habitaciones)
    doc_externo = get_template('modelomedida.html')
    documento = doc_externo.render({"CantidadBaños":obj.get_baños,"CantidadHabitaciones":obj.get_habitaciones,"ValorModelo":obj.Tipo_Modelo})
    return HttpResponse(documento)